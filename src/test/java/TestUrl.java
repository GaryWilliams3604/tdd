import org.junit.Assert;
import org.junit.Test;

public class TestUrl {

    @Test
    public void urlConstructProtocol() {
        String strUrl = "https://launchcode.org/learn";
        Url url = new Url(strUrl);
        Assert.assertEquals("Protocol Test", "https", url.protocol);
    }

    @Test
    public void urlConstructDomain() {
        String strUrl = "https://launchcode.org/learn";
        Url url = new Url(strUrl);
        Assert.assertEquals("Domain Test", "launchcode.org", url.domain);
    }

    @Test
    public void urlConstructPath() {
        String strUrl = "https://launchcode.org/learn?name=ferret&color=purple";
        Url url = new Url(strUrl);
        Assert.assertEquals("Path Test", "/learn", url.path);
    }

    @Test
    public void urlConstructQuery() {
        String strUrl = "https://launchcode.org/learn?name=ferret&color=purple";
        Url url = new Url(strUrl);
        Assert.assertEquals("Query Test", "name=ferret&color=purple", url.query);
    }

    @Test
    public void urlGetProtocol() {
        String strUrl = "https://launchcode.org/learn";
        Url url = new Url(strUrl);
        Assert.assertEquals("Get Protocol Test", "https", url.getProtocol());
    }

    @Test
    public void urlGetDomain() {
        String strUrl = "https://launchcode.org/learn";
        Url url = new Url(strUrl);
        Assert.assertEquals("Get Protocol Test", "launchcode.org", url.getDomain());
    }

    @Test
    public void urlGetPath() {
        String strUrl = "https://launchcode.org/learn";
        Url url = new Url(strUrl);
        Assert.assertEquals("Get Protocol Test", "/learn", url.getPath());
    }

    @Test
    public void urlMixedCaseTestProtocol() {
        String strMixedUrl = "HTTPS://LaunchCode.org/Learn";
        Url url = new Url(strMixedUrl);
        Assert.assertEquals("Protocol Mixed Case Test","https", url.protocol);
    }

    @Test
    public void urlMixedCaseTestDomain() {
        String strMixedUrl = "HTTPS://LaunchCode.org/Learn";
        Url url = new Url(strMixedUrl);
        Assert.assertEquals("Protocol Mixed Case Test","launchcode.org", url.domain);
    }

    @Test
    public void urlMixedCaseTestPath() {
        String strMixedUrl = "HTTPS://LaunchCode.org/Learn";
        Url url = new Url(strMixedUrl);
        Assert.assertEquals("Protocol Mixed Case Test","/learn", url.path);
    }

    @Test
    public void urlOverrideString() {
        String strMixedUrl = "HTTPS://LaunchCode.org/Learn";
        Url url = new Url(strMixedUrl);
        Assert.assertEquals("ToString Override Test", "https://launchcode.org/learn", url.toString());
    }

    @Test (expected = IllegalArgumentException.class)
    public void urlInvalidProtocol() throws Exception {
        String strMixedUrl = "HTPS://LaunchCode.org/Learn";
        Url url = new Url(strMixedUrl);
        Assert.assertEquals("Invalid Protocol Test","https", url.protocol);
    }

    @Test (expected = IllegalArgumentException.class)
    public void urlEmptyProtocol() throws Exception {
        String strMixedUrl = "://LaunchCode.org/Learn";
        Url url = new Url(strMixedUrl);
        Assert.assertEquals("Empty Protocol Test","https", url.protocol);
    }

    @Test (expected = IllegalArgumentException.class)
    public void urlEmptyDomain() throws Exception {
        String strMixedUrl = "https://";
        Url url = new Url(strMixedUrl);
        Assert.assertEquals("Empty Domain Test","launchcode.org", url.domain);
    }

    @Test (expected = IllegalArgumentException.class)
    public void urlDomainCharacters() throws Exception {
        String strMixedUrl = "https://launch^code.org/learn";
        Url url = new Url(strMixedUrl);
        Assert.assertEquals("Domain Characters Test","launchcode.org", url.domain);
    }

}
