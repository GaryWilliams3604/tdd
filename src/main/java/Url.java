
import java.util.ArrayList;

public class Url {
    final String protocol;
    final String domain;
    final String path;
    final String query;


    public Url(String str) {
        ArrayList<String> acceptableProtocols = new ArrayList<>();
        acceptableProtocols.add("ftp");
        acceptableProtocols.add("http");
        acceptableProtocols.add("https");
        acceptableProtocols.add("file");

        String[] urlString = {};
        int protocolIndex = str.indexOf(':', 0);
        String strProtocol = str.substring(0, protocolIndex);
        if (strProtocol.isEmpty()) {
            throw new IllegalArgumentException("Empty Protocol");
        }
        if (acceptableProtocols.contains(strProtocol.toLowerCase())) {
            this.protocol = strProtocol.toLowerCase();
        } else {
            throw new IllegalArgumentException("Invalid Protocol");
        }

        int domainStart = protocolIndex + 3;
        if (domainStart >= str.length()) {
            throw new IllegalArgumentException("Empty Domain");
        }
        int domainEnd = str.indexOf('/', domainStart);
        String strDomain = str.substring(domainStart, domainEnd);
        if (strDomain.isEmpty()) {
            throw new IllegalArgumentException("Empty Domain");
        }
        if (strDomain.matches("[\\w\\.\\-_]+")) {
            this.domain = strDomain.toLowerCase();
        } else {
            throw new IllegalArgumentException("Domain has invalid characters");
        }

        int pathEnd = str.indexOf('?', domainEnd);
        if (pathEnd == -1) {
            pathEnd = str.length();
        }
        String strPath = str.substring(domainEnd, pathEnd);
        this.path = strPath.toLowerCase();

        int queryStartIndex = 0;
        int queryEndIndex = 0;
        if ((queryStartIndex = str.indexOf('?', 0)) == -1) {
            this.query = "";
        } else {
            queryStartIndex++;
            queryEndIndex = str.indexOf('#', queryStartIndex);
            if (queryEndIndex == -1) {
                queryEndIndex = str.length();
            }
            this.query = str.substring(queryStartIndex, queryEndIndex);
        }
    }

    public String getProtocol() {
        return protocol;
    }

    public String getDomain() {
        return domain;
    }

    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return protocol + "://" + domain + path;
    }
}
